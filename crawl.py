# write a simple scraper to get all jobs and company names for all Python jobs from StackOverflow jobs
# the crawler should write out a csv file containing "job title", "product", "company_name", "jobs_count" and print out the total number of jobs
# bonus points if you can consolidate the csv so that multiple jobs from the same company only increment the jobs_count without adding a new row
# bonus points if you write appropriate unit tests

# the base URL is https://stackoverflow.com/jobs
# be careful! they may throttle you!
